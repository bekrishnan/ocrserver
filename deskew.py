# -*- coding: utf-8 -*-

import cv2
import numpy as np
# from scipy.signal import fftconvolve
# from scipy.ndimage import filters, interpolation
import os
import sys

from tesseractservice import TesseractService


"""
Skew detection and correction
"""


class ImgDeskew(object):

    def __init__(self, logger=None):
        
        self.get_deskew_method = 'fast'
        self.rot_cnt = 0
        self.energy_cache = {}
        self.ts = TesseractService()
        self.deskew_image = self.deskew_image_fast
        if self.get_deskew_method == 'slow':
            self.deskew_image = self.deskew_image_slow

    def deskew_image_fast(self, img, binary=False):
        """
        Deskews a given document image

        :param img: input image
        :param binary: boolean flag denoting the input image is binary or not

        :returns: deskewed image
        """
        self.log.debug("deskew: fast method")

        self.log.debug("ConvertColors")
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        if binary:
            # thresh = img.copy()
            thresh = img
        else:
            self.log.debug("threshold")
            ret, thresh = cv2.threshold(gray, 10, 255,
                                        cv2.THRESH_OTSU | cv2.THRESH_BINARY)

        self.log.debug("get rotation")
        angle = self.ts.get_rotation(thresh)

        self.log.info("found angle: %f" % (angle,))

        out = self.rotate_img(thresh, angle)
        # out = self.rotate_img(img, angle)
        # out = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)
        return out

    def rotate_img(self, img, angle):
        self.rot_cnt = self.rot_cnt + 1
        centre = (img.shape[0] / 2, img.shape[1] / 2)
        image_dim = max(img.shape[0], img.shape[1])
        r = cv2.getRotationMatrix2D(centre, angle, 1.0)
        rotated = cv2.warpAffine(img, r, (image_dim, image_dim))
        return rotated

    def deskew_image_slow(self, img, binary=False):
        """
        Deskews a given document image

        :param img: input image
        :param binary: boolean flag denoting the input image is binary or not

        :returns: deskewed image
        """
        self.log.debug("deskew: slow method")

        self.log.debug("ConvertColors")
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        if binary:
            # thresh = gray.copy()
            thresh = img
        else:
            self.log.debug("threshold")
            ret, thresh = cv2.threshold(gray.copy(), 10, 255,
                                        cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)

        self.log.debug("get rotation")
        # checking for maximum 45 degree skew on both directions
        proj_angles = [x for x in range(-45, 46)]

        max_count = -1

        # centre of the image
        centre = (thresh.shape[0] / 2, thresh.shape[1] / 2)

        # dimension of the resultant image
        # As the image is rotated in both directions, the resultant
        # image should have width and height as the greater amongst
        # the original width and height
        image_dim = max(thresh.shape[0], thresh.shape[1])
        best_rm = None

        # iterate through each angle from -45 to +45 degrees and calculate
        # the sum of squares the horizontal projection profile.
        # This is the energy function which we want to optimize.
        # The angle which maximises the energy function is the rotation
        # which we needed to perform for deskewing
        for angle in proj_angles:

            r = cv2.getRotationMatrix2D(centre, angle, 1.0)
            rotated_image = cv2.warpAffine(thresh, r, (image_dim, image_dim))

            # determine the magnitude of the energy function
            hpp_count = self.calc_energy(rotated_image)

            # maximise the energy function and get the corresponding rotated image
            if hpp_count > max_count:
                max_count = hpp_count
                rot_angle = angle
                best_rm = r

        self.log.info("found angle: %f" % (rot_angle,))
        ret, thresh = cv2.threshold(gray, 10, 255,
                                    cv2.THRESH_OTSU | cv2.THRESH_BINARY)
        if rot_angle != 0:
            # self.log.debug("return corrected")
            corrected_image = cv2.warpAffine(thresh, best_rm, (image_dim, image_dim))
            # corrected_image = cv2.warpAffine(img, best_rm, (image_dim, image_dim))
        else:
            # self.log.debug("return original")
            corrected_image = thresh
            # corrected_image = cv2.cvtColor(corrected_image, cv2.COLOR_BGR2GRAY)
        return corrected_image

    def calc_energy(self, img):
        """
        Calculates the energy function as the sum of squares of horizontal
        projection profile

        :param img: input image
        :returns: magnitude of the energy function
        """

        # get the horizontal projection profile
        hpp = np.sum(img, axis=1)

        return np.sum([e ** 2 for e in hpp])
