import ctypes
import ctypes.util
import logging
import numpy as np
import cv2
import tempfile
import os

import oexceptions as exc


class TesseractService(object):

    def __init__(self, logger=None):
        self.log = logger or logging.getLogger(__name__)
        self._load()


    #@config.timeit
    def get_rotation(self, img):
        pix = self.img2pix(img)

        convpix = self.lept.pixConvertTo1(pix, 127)

        angle = ctypes.pointer(ctypes.c_float())
        confidence = ctypes.pointer(ctypes.c_float())

        ret = self.lept.pixFindSkew(convpix, angle, confidence)

        if ret > 0:
           raise exc.ProcessingError("couldn't find angle")

        return -angle.contents.value

    def img2pix(self, img):
        # doing with temporary file because
        # http://stackoverflow.com/questions/26125333/leptonica-unable-to-write-image-after-applying-otsu-threshold
        # http://stackoverflow.com/questions/27000797/converting-mat-to-pix-to-setimage
        # converting opencv's Mat to leptonica's PIX is HARD even when native

        tfd, tname = tempfile.mkstemp(suffix=".png", dir="/dev/shm")
        cv2.imwrite(tname, img)
        pix = self.lept.pixRead(tname)
        os.close(tfd)
        os.unlink(tname)

        return pix



    # @config.timeit
    def _load(self):
        """
        see http://stackoverflow.com/a/21754478/42596
        """
        try:
            libname = ctypes.util.find_library('tesseract')
            tesseract = ctypes.cdll.LoadLibrary(libname)
        except:
            raise exc.ProcessingError("unable to load libtesseract.so")

        try:
            libname = ctypes.util.find_library('lept')
            lept = ctypes.cdll.LoadLibrary(libname)
        except:
            raise exc.ProcessingError("unable to load "+libname)


        class TessStruct(ctypes.Structure): pass
        TessBaseAPI = ctypes.POINTER(TessStruct)

        """
        l_uint32             w;           /* width in pixels                   */
        l_uint32             h;           /* height in pixels                  */
        l_uint32             d;           /* depth in bits (bpp)               */
        l_uint32             spp;         /* number of samples per pixel       */
        l_uint32             wpl;         /* 32-bit words/line                 */
        l_uint32             refcount;    /* reference count (1 if no clones)  */
        l_int32              xres;        /* image res (ppi) in x direction    */
                                          /* (use 0 if unknown)                */
        l_int32              yres;        /* image res (ppi) in y direction    */
                                          /* (use 0 if unknown)                */
        l_int32              informat;    /* input file format, IFF_*          */
        l_int32              special;     /* special instructions for I/O, etc */
        char                *text;        /* text string associated with pix   */
        struct PixColormap  *colormap;    /* colormap (may be null)            */
        l_uint32            *data;        /* the image data                    */
        """
        class PixStruct(ctypes.Structure):
            _fields_ = [
                    ("w", ctypes.c_uint32),
                    ("h", ctypes.c_uint32),
                    ("d", ctypes.c_uint32),
                    ("spp", ctypes.c_uint32),
                    ("wpl", ctypes.c_uint32),
                    ("refcount", ctypes.c_uint32),
                    ("xres", ctypes.c_int32),
                    ("yres", ctypes.c_int32),
                    ("informat", ctypes.c_int32),
                    ("special", ctypes.c_int32),
                    ("text", ctypes.c_char_p),
                    ("colormap", ctypes.c_void_p),
                    ("data", ctypes.POINTER(ctypes.c_uint32)),
                ]

            def __repr__(self):
                return "PIX w: %d, h: %d\ndepth: %d, spp: %d, wpl: %d\n%s" % (self.w, self.h, 
                        self.d, self.spp, self.wpl,
                        #self.data.contents)
                        np.ctypeslib.as_array(self.data, shape=(self.h, self.w)))

        Pix = ctypes.POINTER(PixStruct)

        tesseract.TessBaseAPICreate.restype = TessBaseAPI

        tesseract.TessBaseAPIDelete.restype = None
        tesseract.TessBaseAPIDelete.argtypes = [TessBaseAPI]

        tesseract.TessBaseAPIInit3.argtypes = [TessBaseAPI,
            ctypes.c_char_p,
            ctypes.c_char_p]

        tesseract.TessBaseAPISetImage.restype = None
        tesseract.TessBaseAPISetImage.argtypes = [TessBaseAPI,
            ctypes.c_void_p,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int]

        tesseract.TessBaseAPISetImage2.restype = None
        tesseract.TessBaseAPISetImage2.argtypes = [TessBaseAPI, Pix]

        tesseract.TessBaseAPIGetUTF8Text.restype = ctypes.c_char_p
        tesseract.TessBaseAPIGetUTF8Text.argtypes = [TessBaseAPI]

        tesseract.TessBaseAPISetVariable.restype = ctypes.c_bool
        tesseract.TessBaseAPISetVariable.argtypes = [TessBaseAPI, ctypes.c_char_p, ctypes.c_char_p]

        tesseract.TessBaseAPIReadConfigFile.restype = None
        tesseract.TessBaseAPIReadConfigFile.argtypes = [TessBaseAPI, ctypes.c_char_p]


        self.tesseract = tesseract


        lept.pixCreate.restype = Pix
        lept.pixCreate.argtypes = [ ctypes.c_uint32, ctypes.c_uint32, ctypes.c_uint32 ]

        lept.pixSetData.restype = ctypes.c_int
        lept.pixSetData.argtypes = [ Pix, ctypes.POINTER(ctypes.c_uint32) ]
        lept.pixSetData.argtypes = [ Pix, ctypes.c_void_p ]

        lept.pixRead.restype = Pix
        lept.pixRead.argtypes = [ctypes.c_char_p]

        lept.pixConvertTo1.restype = Pix
        lept.pixConvertTo1.argtypes = [ Pix, ctypes.c_int ]

        lept.pixFindSkew.restype = ctypes.c_int
        lept.pixFindSkew.argtypes = [ Pix, ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_float) ]


        self.lept = lept

if __name__ == '__main__':
    import sys
    import os
    sys.path.append(os.path.realpath('..'))
    sys.path.append(os.path.realpath('.'))
    if len(argv) < 1:
        print "Usage: img vendor"
        sys.exit(2)

    img = sys.argv[1:])
    im = cv2.imread(img)
    tw = TesseractService()
    result = tw.get_rotation(im[:,:,0])
    print result
